import argparse

def get_args():
    parser = argparse.ArgumentParser(prog='COVEX', 
                                     description='COVEX takes raw sequences data to analysis SARS-CoV-2 mutation', 
                                     epilog='Text at the bottom of help.')
    parser.add_argument('-i', '--input',  required=True, 
                        help='Input folder containing fastq files')
    parser.add_argument('-o', '--output', required=True, 
                        help="Output folder for COVmx results")
    parser.add_argument('-b', '--bed', required=True, help="Path to primer.bed file")
    parser.add_argument('-r', '--ref', required=True, help="Path to genome reference sequence")
    args = parser.parse_args()
    return args

