from arguments import get_args 
import subprocess
import os
import pandas as pd
from checker import check_file_existence, check_folder_existence
import sys
import glob
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import random
import numpy as np
import csv
from outbreak_data import authenticate_user
from outbreak_data import outbreak_data
import plotly.graph_objects as go
import matplotlib.patches as patches
from matplotlib.path import Path


#Run FastQC
def run_fastqc(input_folder, output_folder):
    #check if input_folder exit
    if not check_folder_existence(input_folder, "Input"):
        sys.exit(1)
    
    #check if output_folder exist
    if not check_folder_existence(output_folder, "Output"):
        sys.exit(1)
       
    # Check if input folder contains Fastq files
    fastq_files = [file for file in os.listdir(input_folder) if file.endswith(('.fastq', '.fastq.gz', '.fq', '.fq.gz'))]
    if not fastq_files:
        print(f"Error: No Fastq files found in input folder '{input_folder}'.")
        return
    
    # Create 'fastqc' directory within the output folder
    fastqc_output_folder = os.path.join(output_folder, 'fastqc')
    os.makedirs(fastqc_output_folder, exist_ok=True)
    
    for fastq_file in fastq_files:
        fastq_path = os.path.join(input_folder, fastq_file)
        sample_name = fastq_file.split('_')[0]  # Extract sample name from file name
        
        # Create a folder for each sample within the 'fastqc_output' directory
        sample_output_folder = os.path.join(fastqc_output_folder, sample_name)
        os.makedirs(sample_output_folder, exist_ok=True)

        #Run fastQC
        cmd = ['fastqc', '-o', sample_output_folder, fastq_path]
        subprocess.run(cmd)

#Run trim_galore
def run_trim_galore(input_folder, output_folder): 
    # Check if input folder contains Fastq files
    fastq_files = [file for file in os.listdir(input_folder) if file.endswith(('.fastq', '.fastq.gz'))]

    # Create 'trim_galore' directory within the output folder
    trim_galore_output_folder = os.path.join(output_folder, 'trim_galore')
    os.makedirs(trim_galore_output_folder, exist_ok=True)

    for fastq_file in fastq_files:
        fastq_prefix = fastq_file.split('_')[0]
        fastq_path_1 = os.path.join(input_folder, fastq_prefix + '_1.fastq') if fastq_file.endswith('.fastq') else os.path.join(input_folder, fastq_prefix + '_1.fastq.gz')
        fastq_path_2 = os.path.join(input_folder, fastq_prefix + '_2.fastq') if fastq_file.endswith('.fastq') else os.path.join(input_folder, fastq_prefix + '_2.fastq.gz')

        # Create a folder for each sample within the 'trim_galore_output' directory
        sample_output_folder = os.path.join(trim_galore_output_folder, fastq_prefix)
        os.makedirs(sample_output_folder, exist_ok=True)


        #Run trim_galore
        cmd = ['trim_galore', '--paired', fastq_path_1, fastq_path_2,  '-q', '20', '--length', '50', '-o', sample_output_folder, 
               '--illumina']
        subprocess.run(cmd)

def run_fastqc_after_trimming(output_folder):
     # Define the path to the trim_galore_folder
     trim_galore_folder = os.path.join(output_folder, 'trim_galore')
    
     # Check if trim_galore_folder exists
     if not check_folder_existence(trim_galore_folder, "Input"):
         sys.exit(1)
       
     # Get the list of sample directories in the trim_galore folder
     sample_directories = [directory for directory in os.listdir(trim_galore_folder) if os.path.isdir(os.path.join(trim_galore_folder, directory))]
     if not sample_directories:
         print(f"Error: No sample directories found in 'trim_galore' folder '{trim_galore_folder}'.")
         return
    
     # Create 'fastqc_after_trimming' directory within the output folder
     fastqc_after_trimming_output_folder = os.path.join(output_folder, 'fastqc_after_trimming')
     os.makedirs(fastqc_after_trimming_output_folder, exist_ok=True)
    
     for sample_directory in sample_directories:
         sample_folder = os.path.join(trim_galore_folder, sample_directory)
        
         # Get the list of Fastq files in the sample directory
         fastq_files = glob.glob(os.path.join(sample_folder, '*.fq')) + glob.glob(os.path.join(sample_folder, '*.fq.gz'))
         if not fastq_files:
             print(f"Error: No Fastq files found in sample directory '{sample_folder}'.")
             continue
        
         # Create a folder for each sample within the 'fastqc_after_trimming' directory
         sample_output_folder = os.path.join(fastqc_after_trimming_output_folder, sample_directory)
         os.makedirs(sample_output_folder, exist_ok=True)

         # Run FastQC for each Fastq file in the sample directory
         for fastq_file in fastq_files:
            # Run FastQC
             cmd = ['fastqc', '-o', sample_output_folder, fastq_file]
             subprocess.run(cmd)
        

def consensus_generation(bed_file, ref_sequence, output_folder):
    # Check if bed_file exists
    if not check_file_existence(bed_file, "BED"):
        sys.exit(1)
    
    # Check if ref_sequence exists
    if not check_file_existence(ref_sequence, "Reference sequence FASTA"):
        sys.exit(1)
    
    # Define the path to the trim_galore_folder
    trim_galore_folder = os.path.join(output_folder, 'trim_galore')
    
    # Check if trim_galore_folder exists
    if not check_folder_existence(trim_galore_folder, "Input"):
        sys.exit(1)
    
    # Validate BED file format
    bed_df = pd.read_csv(bed_file, sep='\t', header= None)
    expected_columns = ['chrom', 'chromStart', 'chromEnd', 'name', 'primerPool', 'strand', 'sequence']
            
    if len(bed_df.columns) != len(expected_columns):
        print(f"Error: BED file '{bed_file}' does not have the expected number of columns.")
        sys.exit(1)
    if not bed_df.dtypes.tolist() == [object, int, int, object, int, object, object]:
        print(f"Error: BED file '{bed_file}' column types do not match the expected format.")
        sys.exit(1)

    # Validate reference sequence FASTA format
    with open(ref_sequence, 'r') as fasta_file:
         line = fasta_file.readline()
         if not line.startswith('>'):
             print(f"Error: Reference sequence FASTA file '{ref_sequence}' is not in the expected format.")
             sys.exit(1)
    

    # Get the list of sample directories in the trim_galore folder
    sample_directories = [directory for directory in os.listdir(trim_galore_folder) if os.path.isdir(os.path.join(trim_galore_folder, directory))]
    if not sample_directories:
         print(f"Error: No sample directories found in 'trim_galore' folder '{trim_galore_folder}'.")
         return
    
    # Create 'consensus_sequence' directory within the output folder
    consensus_sequence_output_folder = os.path.join(output_folder, 'consensus_sequences')
    os.makedirs(consensus_sequence_output_folder, exist_ok=True)
    
    for sample_directory in sample_directories:
         sample_folder = os.path.join(trim_galore_folder, sample_directory)
        
         # Get the list of Fastq files in the sample directory
         fastq_files = [file for file in os.listdir(sample_folder) if file.endswith(('.fq', '.fq.gz'))]
         if not fastq_files:
             print(f"Error: No Fastq files found in sample directory '{sample_folder}'.")
             continue
        
         for fastq_file in fastq_files:
             fastq_prefix = fastq_file.split('_')[0]
             #fastq_prefix = fastq_file.rsplit('_', 1)[0]
             fastq_path_1 = os.path.join(sample_folder, fastq_prefix + '_1_val_1.fq') if fastq_file.endswith('.fq') else os.path.join(sample_folder, fastq_prefix + '_1_val_1.fq.gz')
             fastq_path_2 = os.path.join(sample_folder, fastq_prefix + '_2_val_2.fq') if fastq_file.endswith('.fq') else os.path.join(sample_folder, fastq_prefix + '_2_val_2.fq.gz') 
             sampleName = os.path.splitext(os.path.basename(fastq_file))[0]
         # Create a folder for each sample within the 'consensus_sequence_output' directory
         sample_output_folder = os.path.join(consensus_sequence_output_folder, fastq_prefix)
         os.makedirs(sample_output_folder, exist_ok=True)

      
         # Index reference
         index_command = ['bwa', 'index', ref_sequence] 
         subprocess.run(index_command)
  
         # Run BWA and create a SAM file
         sample_sam_file = os.path.join(sample_output_folder, f'{fastq_prefix}.sam')
         alignment_command = ['bwa', 'mem', '-t 4', ref_sequence, fastq_path_1, fastq_path_2]

         with open(sample_sam_file, 'w') as sam_output_file:
             subprocess.run(alignment_command, stdout=sam_output_file)

         # Convert SAM file to BAM file 
         sample_bam_file = os.path.join(sample_output_folder, f'{fastq_prefix}.bam')
         convert_to_bam = ['samtools', 'view', '-bS', '-o', sample_bam_file, sample_sam_file]
         subprocess.run(convert_to_bam)

         #Index BAM file
         index_bam = ['samtools', 'index', sample_bam_file]
         subprocess.run(index_bam)

         #Primer trimming
         sample_trim_bam_file = os.path.join(sample_output_folder, f'{fastq_prefix}_trim.bam')
         primer_trimming = ['ivar', 'trim', '-e', '-i', sample_bam_file, '-b', bed_file, '-m',  '30', '-q', '20', '-p', sample_trim_bam_file]
         subprocess.run(primer_trimming)

        #Sort BAM file
         sample_sort_trim_bam_file = os.path.join(sample_output_folder, f'{fastq_prefix}_trim_sort.bam')
         sort_trim_bam_file = ['samtools', 'sort', '-@4', sample_trim_bam_file, '-o', sample_sort_trim_bam_file ]
         subprocess.run(sort_trim_bam_file)

         #Index trim BAM file
         index_trim_bam_file = ['samtools', 'index', sample_sort_trim_bam_file]
         subprocess.run(index_trim_bam_file)

         #Generate consensus sequence
         consensus_sequence_output = os.path.join(sample_output_folder, f'{fastq_prefix}.fa')
         mpileup_command = ['samtools', 'mpileup', '-aa', '-A', '-d', '0', '-Q', '0', sample_sort_trim_bam_file]
         ivar_consensus_command = ['ivar', 'consensus', '-p', consensus_sequence_output, '-t', '0.75', '-m', '10']
         mpileup_process = subprocess.Popen(mpileup_command, stdout=subprocess.PIPE)
         ivar_consensus_process = subprocess.Popen(ivar_consensus_command, stdin=mpileup_process.stdout)
         mpileup_process.stdout.close()  # Close the stdout of mpileup process
         ivar_consensus_process.communicate()  # Wait for ivar consensus process to finish


          
def mutation_call(output_folder):
    # Define the path to the consensus_sequence folder
    consensus_sequence_folder = os.path.join(output_folder, 'consensus_sequences')

    # Check if consensus_sequence folder exists
    if not check_folder_existence(consensus_sequence_folder, "Input"):
        sys.exit(1)
       
     # Get the list of sample directories in the consensus_sequence folder
    sample_directories = [directory for directory in os.listdir(consensus_sequence_folder) if os.path.isdir(os.path.join(consensus_sequence_folder, directory))]
    if not sample_directories:
        print(f"Error: No sample directories found in 'consensus_sequences' folder '{consensus_sequence_folder}'.")
        return
    
     # Create 'nextclade' directory within the output folder
    nextclade = os.path.join(output_folder, 'nextclade')
    os.makedirs(nextclade, exist_ok=True)
    
    for sample_directory in sample_directories:
        sample_folder = os.path.join(consensus_sequence_folder, sample_directory)
        
        # Get the list of Fasta files in the sample directory
        fasta_files = glob.glob(os.path.join(sample_folder, '*.fa')) + glob.glob(os.path.join(sample_folder, '*.fasta'))
        if not fasta_files:
            print(f"Error: No Fasta files found in sample directory '{sample_folder}'.")
            continue
        
        # Create a folder for each sample within the 'nextclade' directory
        sample_output_folder = os.path.join(nextclade, sample_directory)
        os.makedirs(sample_output_folder, exist_ok=True)

        download_datased = ['nextclade', 'dataset', 'get', '--name', 'sars-cov-2', '--output-dir', nextclade]
        subprocess.run(download_datased)
        # Run nextclade for each Fasta file in the sample directory
        
        for fasta_file in fasta_files:
             # Run FastQC
             call = ['nextclade', 'run', '--input-dataset', nextclade, '--output-all', sample_output_folder, fasta_file]
             subprocess.run(call)
        


def pango_designation(output_folder):
     # Define the path to the consensus_sequence folder
    consensus_sequence_folder = os.path.join(output_folder, 'consensus_sequences')

    # Check if consensus_sequence folder exists
    if not check_folder_existence(consensus_sequence_folder, "Input"):
        sys.exit(1)
       
     # Get the list of sample directories in the consensus_sequence folder
    sample_directories = [directory for directory in os.listdir(consensus_sequence_folder) if os.path.isdir(os.path.join(consensus_sequence_folder, directory))]
    if not sample_directories:
        print(f"Error: No sample directories found in 'consensus_sequences' folder '{consensus_sequence_folder}'.")
        return
    
     # Create 'Pango' directory within the output folder
    pango_output_folder = os.path.join(output_folder, 'pangolin')
    os.makedirs(pango_output_folder, exist_ok=True)
    
    for sample_directory in sample_directories:
        sample_folder = os.path.join(consensus_sequence_folder, sample_directory)
        sample_name = sample_folder.split("/")[-1]
        
        # Get the list of Fasta files in the sample directory
        fasta_files = glob.glob(os.path.join(sample_folder, '*.fa')) + glob.glob(os.path.join(sample_folder, '*.fasta'))
        if not fasta_files:
            print(f"Error: No Fasta files found in sample directory '{sample_folder}'.")
            continue

         # Run pango for each Fasta file in the sample directory
        for fasta_file in fasta_files:
            # Extract the sample name from the fasta_file
            sample_name = os.path.splitext(os.path.basename(fasta_file))[0]

            # Create the output directory for the sample
            sample_pango_output_folder = os.path.join(pango_output_folder, sample_name)
            os.makedirs(sample_pango_output_folder, exist_ok=True)
            # Run Pangolin for the current FASTA file
            cmd = ['pangolin', fasta_file, '--outdir', sample_pango_output_folder, '--outfile', f"{sample_name}.csv"]
            subprocess.run(cmd)


def mut_diagram(output_folder):
     # Define the path to the nextclade folder
    nextclade_folder = os.path.join(output_folder, 'nextclade')

     # Check if nextclade folder exists
    if not check_folder_existence(nextclade_folder, "Input"):
         sys.exit(1)

     # Get the list of sample directories in the SPEAR folder
    sample_directories = [directory for directory in os.listdir(nextclade_folder) if os.path.isdir(os.path.join(nextclade_folder, directory))]
    if not sample_directories:
         print(f"Error: No sample directories found in 'nextclade' folder '{nextclade_folder}'.")
         return

     # Create 'plot' directory within the output folder
    plot_output_folder = os.path.join(output_folder, 'lolliplot')
    os.makedirs(plot_output_folder, exist_ok=True)

    for sample_directory in sample_directories:
        sample_folder = os.path.join(nextclade_folder, sample_directory)

        # Get the list of TSV files in the sample directory
        tsv_files = glob.glob(os.path.join(sample_folder, '*.tsv'))
        if not tsv_files:
            print(f"Error: No TSV files found in sample directory '{sample_folder}.")
            exit()

        # Run plot for each TSV file in the sample directory
        for tsv_file in tsv_files:
            # Create the output directory for the sample
            sample_plot_output_folder = os.path.join(plot_output_folder, os.path.basename(sample_folder))
            os.makedirs(sample_plot_output_folder, exist_ok=True)

            # Read the TSV file
            annot = pd.read_csv(tsv_file, sep="\t")
            aachange = annot['aaSubstitutions']

            sample_gr = {}
            for i in range(len(aachange)):
                sample_id = f"Sample_{i}"
                sample_gr[sample_id] = {
                    'aachange': aachange[i]
                }

            gene_list = []
            aachange_list = []
            aaposition_list = []

            for sample_id, sample_data in sample_gr.items():
                aachanges = sample_data['aachange']
                split_aachange = aachanges.split(",")

                for entry in split_aachange:
                    gene, aachange = entry.split(":")
                    gene_list.append(gene)
                    aachange_list.append([aachange])  # Wrap aachange in a list
                    position = int(''.join(filter(str.isdigit, aachange)))
                    aaposition_list.append([position])  # Wrap position in a list

            features = {
                'ORF1a': {'start': 1, 'width': 4401, 'color': 'firebrick'},
                'ORF1b': {'start': 4402, 'width': 2696, 'color': 'lightsalmon'},
                'S': {'start': 7099, 'width': 1274, 'color': 'wheat'},
                'ORF3a': {'start': 8374, 'width': 276, 'color': 'tan'},
                'E': {'start': 8651, 'width': 76, 'color': 'goldenrod'},
                'M': {'start': 8728, 'width': 223, 'color': 'dimgrey'},
                'ORF6': {'start': 8952, 'width': 62, 'color': 'silver'},
                'ORF7a': {'start': 9014, 'width': 122, 'color': 'rosybrown'},
                'ORF7b': {'start': 9137, 'width': 44, 'color': 'orangered'},
                'ORF8': {'start': 9182, 'width': 122, 'color': 'indianred'},
                'ORF9b': {'start': 9305, 'width': 98, 'color': 'brown'},
                'N': {'start': 9404, 'width': 420, 'color': 'darkred'},
            }

            # Get the x-values and the corresponding gene names
            x_values = []
            gene_names = []
            stemline_labels = []  # List to store the stemline labels
            stemline_positions = []  # List to store the stemline positions
            for i, gene in enumerate(gene_list):
                if gene in features:
                    feature = features[gene]
                    positions = aaposition_list[i]
                    changes = aachange_list[i]
                    for position, change in zip(positions, changes):
                        x = feature['start'] + position
                        x_values.append(x)
                        gene_names.append(gene)
                        stemline_labels.append(change)  # Add the amino acid change as the stemline label
                        stemline_positions.append((x, random.uniform(0.2, 0.7)))  # Add the stemline position with random height

            # Create a Stem plot with circles as markers
            fig = go.Figure()

            # Add stemlines and lines connecting stems to features
            for i, pos in enumerate(stemline_positions):
                x_stem, y = pos
                x_feature = x_stem
                gene = gene_names[i]
                color = 'black'

                # Add stemline
                fig.add_trace(go.Scatter(
                    x=[x_stem, x_stem],
                    y=[y, 0.08],
                    mode='lines',
                    line=dict(
                        color=color,
                        width=5
                    ),
                    hoverinfo='none',
                    showlegend=False
                ))

                # Add circle marker at the stemline position
                fig.add_trace(go.Scatter(
                    x=[x_stem],
                    y=[y],
                    mode='markers',
                    marker=dict(
                        symbol='circle',
                        size=40,
                        color='black'
                    ),
                    hoverinfo='none',
                    showlegend=False
                ))

                # Add line connecting stem to feature
                fig.add_trace(go.Scatter(
                    x=[x_stem, x_feature],
                    y=[0.08, 0.08],
                    mode='lines',
                    line=dict(
                        color=color,
                        width=6,
                        dash='dash'
                    ),
                    hoverinfo='none',
                    showlegend=False
                ))


                # Add stemline label
                fig.add_annotation(
                    x=x_stem,
                    y=y,
                    text='<b>' + stemline_labels[i][-1] + '</b>',  # Use HTML tags for bold formatting
                    hovertext='<b>Change:</b> ' + stemline_labels[i] + '<br><b>Gene:</b> ' + gene,
                    showarrow=False,
                    font=dict(
                        size=30,
                        color='white'
                    )
                )


            # Add feature rectangles
            for feature, feature_data in features.items():
                start = feature_data['start']
                width = feature_data['width']
                color = feature_data['color']
                fig.add_shape(
                    type='rect',
                    xref='x', yref='paper',
                    x0=start, y0=0.08, x1=start + width, y1=0,
                    fillcolor=color, line=dict(color='black')
                )

            # Customize the layout
            fig.update_layout(
                yaxis=dict(
                    range=[0, 1.2],
                    visible=False
                ),

                xaxis=dict(
                    tickmode='array',
                    tickvals=x_values,
                    ticktext=gene_names,
                    tickfont=dict(
                        size=25  # Increase the font size of the feature rectangles text
                    )
                ),

                hoverlabel=dict(
                    bgcolor='white',
                    font_size=15,
                    font_family='Arial'
                )
            )

            # Save the interactive plot as an HTML file
            figure_path = os.path.join(sample_plot_output_folder, 'interactive_plot.html')
            fig.write_html(figure_path)

def outbreak_lineage(output_folder):
     # Define the path to the pango folder
    pango_folder = os.path.join(output_folder, 'pangolin')

     # Check if pango folder exists
    if not check_folder_existence(pango_folder, "Input"):
         sys.exit(1)

     # Get the list of sample directories in the pango folder
    sample_directories = [directory for directory in os.listdir(pango_folder) if os.path.isdir(os.path.join(pango_folder, directory))]
    if not sample_directories:
         print(f"Error: No sample directories found in 'Pangolin' folder '{pango_folder}'.")
         return

    # Create 'outbreak' directory within the output folder
    outbreak_output_folder = os.path.join(output_folder, 'outbreak')
    os.makedirs(outbreak_output_folder, exist_ok=True)

    authenticate_user.authenticate_new_user()

    lineage_values = [] 

    for sample_directory in sample_directories:
        sample_folder = os.path.join(pango_folder, sample_directory)
        sample_name = sample_folder.split("/")[-1]

         # Get the list of CSV files in the sample directory
        csv_files = glob.glob(os.path.join(sample_folder, '*.csv'))
        if not csv_files:
            print(f"Error: No CSV files found in sample directory '{sample_folder}.")
            continue

        for csv_file in csv_files:
             # Create the output directory for the sample
            sample_outbreak_output_folder = os.path.join(outbreak_output_folder, sample_directory)
            os.makedirs(sample_outbreak_output_folder, exist_ok=True)
            with open(csv_file, "r") as file:
                 # Create a CSV reader object
                reader = csv.reader(file)
                 # Read the first row (assuming only one row in the CSV)
                next(reader)
                row = next(reader)
                 # Extract the lineage value from the row
                lineage_value = row[1]               
                df = outbreak_data.lineage_mutations(lineage_value)
                 # Save the DataFrame as a CSV file in the output folder
                output_file = os.path.join(sample_outbreak_output_folder, f"{sample_name}_lineage_prevalence.csv")
                df.to_csv(output_file, index=False)
                lineage_values.append(lineage_value)  # Append lineage value as a separate list

    lineage_df = pd.DataFrame({"lineage": lineage_values})  # Create DataFrame from the list
    lineage_df = lineage_df.explode("lineage", ignore_index=True)  # Explode the list into separate rows

    # Define the path to the nextclade folder
    nextclade_folder = os.path.join(output_folder, 'nextclade')

    # Get the list of sample directories in the nextclade folder
    sample_directories_nextclade = [directory for directory in os.listdir(nextclade_folder) if os.path.isdir(os.path.join(nextclade_folder, directory))]

    values_aa = []
    for sample_directory_nextclade in sample_directories_nextclade:
        sample_folder_nextclade = os.path.join(nextclade_folder, sample_directory_nextclade)

            # Get the list of tsv files in the sample directory
        tsv_files = glob.glob(os.path.join(sample_folder_nextclade, '*.tsv'))
        #sample_name = os.path.basename(sample_directory_nextclade)
        for tsv_file in tsv_files:
            # Read the TSV file
            annot = pd.read_csv(tsv_file, sep="\t")
            aachange = annot['aaSubstitutions']
            sample_gr = {}
            for i in range(len(aachange)):
                sample_id = f"Sample_{i}"
                sample_gr[sample_id] = {
                    'aachange': aachange[i]
                }
            value_aa = sample_gr['Sample_0']['aachange']
            values_aa.append(value_aa)

    aa_df = pd.DataFrame({"aachange": values_aa})  # Create DataFrame from the list
    aa_df = aa_df.explode("aachange", ignore_index=True)  # Explode the list into separate rows

    for aa, lineage, sample_directory in zip(aa_df['aachange'], lineage_df['lineage'], sample_directories):
        aa_values = aa.split(",")
        lineage = lineage.lower()
        df_combined_aa = pd.DataFrame()  # Initialize an empty DataFrame for each aa value

        for aa_value in aa_values:
            try:
                df_prev = outbreak_data.mutations_by_lineage(aa_value.strip())
                df_prev['pangolin_lineage'] = df_prev['pangolin_lineage'].apply(lambda x: x == lineage)
                if not df_prev['pangolin_lineage'].any():
                    df_prev = pd.DataFrame({
                        "pangolin_lineage": [0],
                        "lineage_count": 0,
                        "mutation_count": 0,
                        "proportion": 0,
                        "proportion_ci_lower": 0,
                        "proportion_ci_upper": 0
                    })
                else:
                    df_prev = df_prev[df_prev['pangolin_lineage']]
            except NameError:
                # Handle the error by setting values as 0
                df_prev = pd.DataFrame({
                    "pangolin_lineage": [0],
                    "lineage_count": 0,
                    "mutation_count": 0,
                    "proportion": 0,
                    "proportion_ci_lower": 0,
                    "proportion_ci_upper": 0
                })
            
            # Add mutation and lineage columns
            df_prev['mutation'] = aa_value.strip()
            df_prev['lineage'] = lineage
            df_combined_aa = pd.concat([df_combined_aa, df_prev])  # Concatenate df_prev with the combined DataFrame for the aa value


        sample_folder = os.path.join(pango_folder, sample_directory)
        sample_name = sample_folder.split("/")[-1]
        sample_outbreak_output_folder = os.path.join(outbreak_output_folder, sample_directory)
        os.makedirs(sample_outbreak_output_folder, exist_ok=True)
        output_prev = os.path.join(sample_outbreak_output_folder, f"{sample_name}_mutation_prevalence.csv")
        df_combined_aa.to_csv(output_prev, index=False)  # Save the combined DataFrame in the sample directory


def outbreak_heatmap(output_folder):
    # Define the path to the outbreak folder
    outbreak_folder = os.path.join(output_folder, 'outbreak')

     # Check if outbreak folder exists
    if not check_folder_existence(outbreak_folder, "Input"):
         sys.exit(1)

     # Get the list of sample directories in the pango folder
    sample_directories = [directory for directory in os.listdir(outbreak_folder) if os.path.isdir(os.path.join(outbreak_folder, directory))]
    if not sample_directories:
         print(f"Error: No sample directories found in 'outbreak' folder '{outbreak_folder}'.")
         return
    all_data = pd.DataFrame()
    for sample_directory in sample_directories:
        sample_folder = os.path.join(outbreak_folder, sample_directory)
        sample_name = os.path.basename(sample_folder)

        # Get the list of tsv files in the sample directory
        csv_files_gisaid = glob.glob(os.path.join(sample_folder, '*_lineage_prevalence.csv'))
        if not csv_files_gisaid:
            print(f"Error: No CSV files found in sample directory '{sample_folder}.")
            continue
       
        # Run outbreak for each csv file in the sample directory
        for csv_file_gisaid in csv_files_gisaid:
            # Create the output directory for the sample
            df = pd.read_csv(csv_file_gisaid, sep=",")
            df['sample'] = sample_name
            df['analysis_type'] = 'GISAID report'
            all_data = pd.concat([df, all_data], ignore_index=True)


         # Get the list of csv files in the sample directory
        csv_files_sample = glob.glob(os.path.join(sample_folder, '*_mutation_prevalence.csv'))
        #sample_name = os.path.basename(sample_directory_spear)
        for csv_file_sample in csv_files_sample:
            # Read the TSV file
            mut_sample = pd.read_csv(csv_file_sample, sep=",")
            mut_sample['sample'] = sample_name
            mut_sample['analysis_type'] = 'Sample report'
            mut_sample = mut_sample.rename(columns={'proportion': 'prevalence'})
            mut_sample['codon_num'] = mut_sample['mutation'].apply(lambda x: int(x.split(':')[1][1:-1]) if pd.notnull(x) and ':' in x else None)
            data = mut_sample
            all_data = pd.concat([data, all_data], ignore_index=True)
            all_data['mutation'] = all_data['mutation'].str.upper()
            all_data['gene'] = all_data['mutation'].apply(lambda x: x.split(':')[0] if pd.notnull(x) else x)
    
    sample_names = all_data["sample"].unique()
    for sample_name in sample_names:
        filtered_data = all_data.loc[all_data['sample'] == sample_name]
        filtered_data = filtered_data[['sample', 'lineage', 'mutation', 'gene', 'prevalence', 'codon_num', 'analysis_type']]
        gene_order = ['ORF1A', 'ORF1B', 'S', 'ORF3A', 'E', 'M', 'ORF6', 'ORF7A', 'ORF7B', 'ORF8', 'ORF9B','N', 'ORF10']
        filtered_data = filtered_data[filtered_data['gene'].isin(gene_order)]
        filtered_data['gene'] = pd.Categorical(filtered_data['gene'], categories=gene_order, ordered=True)
        filtered_data = filtered_data.sort_values(by=['gene', 'codon_num'])
        lineage_name = filtered_data.loc[filtered_data['lineage'].notnull(), 'lineage'].iloc[0]  # Extract lineage value

        heatmap_data = filtered_data.pivot_table(index='analysis_type', columns=['mutation'], values='prevalence')
        heatmap_data = heatmap_data.reindex(columns=filtered_data['mutation'].unique())

        heatmap_data *= 100

        # Generate heatmap using matplotlib
        fig, ax = plt.subplots(figsize=(13, 6))
        heatmap = ax.imshow(heatmap_data, cmap='RdBu')

        # Add lines between squares
        ax.set_xticks(np.arange(heatmap_data.shape[1] + 1) - 0.5, minor=True)
        ax.set_yticks(np.arange(heatmap_data.shape[0] + 1) - 0.5, minor=True)
        ax.grid(which='minor', color='lightgray', linestyle='-', linewidth=1)

        # Add perpendicular lines in missing values
        mask = np.isnan(heatmap_data)
        for i in range(heatmap_data.shape[0]):
            for j in range(heatmap_data.shape[1]):
                if mask.iloc[i, j]:
                    rect = plt.Rectangle((j - 0.5, i - 0.5), 1, 1, fill=False, edgecolor='gainsboro', hatch='//')
                    ax.add_patch(rect)

        # Add colorbar
        cbar = plt.colorbar(heatmap)
        cbar.set_label('Prevalence (%)')

        # Set labels and title
        ax.set_xticks(np.arange(heatmap_data.shape[1]))
        ax.set_yticks(np.arange(heatmap_data.shape[0]))
        ax.set_xticklabels(heatmap_data.columns, rotation='vertical')
        ax.set_xticklabels(heatmap_data.columns)
        ax.set_yticklabels(heatmap_data.index)
        plt.xlabel('Mutation')
        plt.ylabel(lineage_name.upper())
        plt.title('Mutation Prevalence Heatmap')

        # Save the heatmap plot
        figure_path = os.path.join(f"{outbreak_folder}/{sample_name}/{sample_name}.png")
        plt.savefig(figure_path, dpi=450)
        plt.close()

            
           
if __name__ == '__main__':
    args = get_args()  
    run_fastqc(args.input, args.output)
    run_trim_galore(args.input, args.output)
    run_fastqc_after_trimming(args.output)
    consensus_generation(args.bed, args.ref, args.output)
    mutation_call(args.output)
    pango_designation(args.output)
    mut_diagram(args.output)
    outbreak_lineage(args.output)
    outbreak_heatmap(args.output)
  
       

