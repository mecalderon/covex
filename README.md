# CoVEx

# SARS-CoV-2 Mutation Explorer

## Introduction

The SARS-CoV-2 Mutation Explorer (CoVEx) is a user-friendly tool designed to analyze and visualize SARS-CoV-2 variants using Illumina sequencing data. Its primary purpose is to compare the mutations identified in a given sample with the mutation profile documented for the corresponding lineage in GISAID.

CoVEx offers the flexibility to process single or multiple input files and accepts both single-end and paired-end raw sequences. The tool generates an interactive report that presents the identified mutations in a clear and intuitive manner. Additionally, CoVEx generates a visually informative heatmap, illustrating the mutation profile reported on GISAID alongside the mutations identified in the sample.

## Installation

Clone this repo: 

`git clone https://gitlab.com/CNCA_CeNAT/covex`

Change to downloaded directory:

`cd covex`

Run the install script, (this requires a working conda install, we recommend [miniconda](https://docs.conda.io/en/latest/miniconda.html)):

`conda env create -f environment.yml`

Activate the conda environment:

`conda activate covex`

Run CoVEx:

`python CoVEx.py`


## Usage

To run CoVEx, you need an input folder that contains the Fastq files. The raw sequence files should be in one of the following formats: '.fastq', '.fastq.gz', '.fq', or '.fq.gz'. It is mandatory to specify the paths to both the primer bed file and the genome reference sequence.

The primer bed file should be in a specific format, including the following columns: 'chrom', 'chromStart', 'chromEnd', 'name', 'primerPool', 'strand', and 'sequence'. When selecting the genome reference sequence, you can choose either [NC_045512.2](https://www.ncbi.nlm.nih.gov/nuccore/1798174254) or [MN908947.3](https://www.ncbi.nlm.nih.gov/nuccore/MN908947.3) as your reference.

```
usage: python CoVEx.py [-h] -i INPUT -o OUTPUT -b BED -r REF

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Input folder containing fastq files
  -o OUTPUT, --output OUTPUT
                        Output folder for COVmx results
  -b BED, --bed BED     Path to primer.bed file
  -r REF, --ref REF     Path to genome reference sequence

```

## Usage examples

To check installation was successful. View the example, run:

`python CoVEx.py -i data/example_fastq -o data/results -b data/v1.bed -r data/NC_045512.2.fasta`

This will save outputs including the HTML report and the prevalence heatmap to the `data/results` directory within your current working directory.

### Software dependencies used

CoVEx makes use of the following:

* [conda](https://docs.conda.io/en/latest/index.html)
* [bioconda](https://bioconda.github.io/) 
* [fastqc] (https://github.com/s-andrews/FastQC)
* [trim-galore] (https://github.com/FelixKrueger/TrimGalore)
* [bwa] (https://github.com/lh3/bwa)
* [samtools] (https://github.com/samtools/samtools)
* [ivar] (https://github.com/andersen-lab/ivar)
* [pip] (https://pip.pypa.io/en/stable/installation/)
* [pangolin] (https://github.com/cov-lineages/pangolin)
* [nextclade] (https://github.com/nextstrain/nextclade)
* [matplotlib] (https://anaconda.org/conda-forge/matplotlib)
* [plotly] (https://anaconda.org/plotly/plotly)
* [python-outbreak-info] (https://github.com/outbreak-info/python-outbreak-info)

## References

1. [S. Andrews, "FastQC: A Quality Control tool for High Throughput Sequence Data," 2010.](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
2. [F. Krueger, "Trim Galore," 2012.](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/)
3. [ H. Li and R. Durbin, "Fast and accurate short read alignment with Burrows-Wheeler transform," Bioinformatics, vol. 25, no. 14, pp. 1754-1760, Jul. 2009.]
4. [P. Danecek, J.K. Bonfield, J. Liddle, J. Marshall, V. Ohan, M.O. Pollard, A. Whitwham, T. Keane, S.A. McCarthy, R.M. Davies, and H. Li, "Twelve years of SAMtools and BCFtools," GigaScience, vol. 10, no. 2, Feb. 2021, giab008](https://doi.org/10.1093/gigascience/giab008).
5. [N.D. Grubaugh, K. Gangavarapu, J. Quick, N.L. Matteson, J.G. de Jesus, B.J. Main, A.L. Tan, L.M. Paul, D.E. Brackney, S. Grewal, et al., "An amplicon-based sequencing framework for accurately measuring intrahost virus diversity using PrimalSeq and iVar," Genome Biol., vol. 20, no. 1, p. 8, Jan. 2019.]
6. [I. Aksamentov, C. Roemer, E.B. Hodcroft, and R.A. Neher, "Nextclade: clade assignment, mutation calling and quality control for viral genomes," Journal of Open Source Software, vol. 6, no. 67, p. 3773, 2021.](https://doi.org/10.21105/joss.03773)
7. [Á. O'Toole, E. Scher, A. Underwood, B. Jackson, V. Hill, J.T. McCrone, R. Colquhoun, C. Ruis, K. Abu-Dahab, B. Taylor, et al., "Assignment of epidemiological lineages in an emerging pandemic using the pangolin tool," Virus Evol., vol. 7, no. 2, veab064, Feb. 2021.]
8. [J.D. Hunter, "Matplotlib: A 2D Graphics Environment," Computing in Science \& Engineering, vol. 9, no. 3, pp. 90-95, May-June 2007.]
9. [M. Alkuzweny, K. Gangavarapu, and L. Hughes, "outbreakinfo: outbreak.info R Client," Python package version 1.0.1, Mar. 2022.](https://github.com/outbreak-info/python-outbreak-info).
